//
//  BaseRequest.swift
//  SwiftDocC
//
//  Created by Ray Chen on 2023/8/24.
//

import Foundation

/// 解析錯誤資訊
struct ErrorResponse: Decodable {
    let code: Int
    let message: String?
}

/// Request 物件
class BaseRequest<Response>: Operation {
    
    /// API 方法
    enum HttpMethod: String {
        case post = "POST"
        case get = "GET"
        case delete = "DELETE"
        case put = "PUT"
        case patch = "PATCH"
    }
    
    /// response call back
    var completionHandler: ((Swift.Result<Response, Error>) -> Void)?
    
    private var task: URLSessionDataTask?
    private var httpMethod: HttpMethod = .get
    private let path: String
    private var queryItems: [URLQueryItem]?
    private var body: Data?
    
    init(path: String, method: HttpMethod) {
        self.httpMethod = method
        self.path = path
    }
    
    func setQueryItems(_ queryItems: [URLQueryItem]) {
        self.queryItems = queryItems
    }
    
    func setupRequestBody(_ parameter: [String: Any]) {
        let data = try? JSONSerialization.data(withJSONObject: parameter, options: .fragmentsAllowed)
        self.body = data
    }
    
    func setupRequestBody(data: Data) {
        self.body = data
    }
    
    func decode(data: Data) -> Response? {
        //for implementing
        nil
    }

    func requestDidSuccess(response: Response) {
        //for implementing
    }

    func debug(data: Data?, response: URLResponse?, error: Error?) {
        //for debugging
    }
    
    private func decodeError(data: Data) -> ErrorResponse? {
        guard let error = try? JSONDecoder().decode(ErrorResponse.self, from: data) else { return nil }
        print("error code: \(error.code), message = \(error.message ?? "")")
        return error
    }
    
    override func cancel() {
        task?.cancel()
        super.cancel()
    }
    
    override func main() {
        guard !isCancelled else { return }
        let host: String = ""
        var urlComponents: URLComponents? = .init(string: host)
        urlComponents?.path = path
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else {
            let error = converError(code: 404, message: "無法取的正確連結")
            completionHandler?(.failure(error))
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        request.httpBody = body
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30
        let session = URLSession(configuration: config, delegate: nil, delegateQueue: nil)
        let semaphore = DispatchSemaphore(value: 0)
        
        let task = session.dataTask(with: request) { data, response, error in
            self.debug(data: data, response: response, error: error)
            if let error = error as? NSError {
                self.completionHandler?(.failure(error))
            } else if let data = data {
                if let model = self.decodeError(data: data) {
                    let error = self.converError(code: model.code, message: model.message ?? "")
                    self.completionHandler?(.failure(error))
                } else if let model = self.decode(data: data) {
                    self.requestDidSuccess(response: model)
                    self.completionHandler?(.success(model))
                } else {
                    let error = self.converError(code: 404, message: "無法解析")
                    self.completionHandler?(.failure(error))
                }
            }
            semaphore.signal()
        }
        task.resume()
        session.finishTasksAndInvalidate()
        self.task = task
        semaphore.wait()
    }
    
    /// 轉換成 Error 物件
    /// - Parameters:
    ///   - code: 錯誤代碼
    ///   - message: 錯誤資訊
    /// - Returns: Error
    func converError(code: Int, message: String) -> Error {
        let error = NSError(domain: "showDocC", code: code, userInfo: [:])
        return error
    }
}

