//
//  ViewController.swift
//  SwiftDocC
//
//  Created by Ray Chen on 2023/8/24.
//

import UIKit

/// 首頁
class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    /// 登入
    /// - Parameters:
    ///   - account: 使用者帳號
    ///   - password: 使用者密碼
    func login(account: String, password: String) {
        
    }
    
    /// 顯示錯誤訊息
    /// - Parameter error: 錯誤資訊
    func showError(error: Error) {
        
    }
}

